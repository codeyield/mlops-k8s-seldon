# Демонстрационный проект MLOps

Этот репозиторий является 1/2 частью демонстрации pet-проекта MLOps, которая включает развертывание кластера Kubernetes, системы машинного обучения Seldon Core, необходимые зависимости и CI/CD пайплайны.

Другая ML-часть проекта в этом репозитории https://gitlab.com/codeyield/MLOps-ML-LLM

## Основные сущности

- Преднастроенный легкий кластер Kubernetes на базе [Microk8s](https://microk8s.io/)
- Система машинного обучения [Seldon Core](https://www.seldon.io/)

## Развертывание

### Развертывание кластера Kubernetes

Развернем простой кластер Kubernetes и установим следующие зависимости:

- [Microk8s](docs/microk8s.md) - легкая версия Kubernetes
- [Nginx](docs/nginx.md) - популярный веб-сервер (требуется для Ingress)
- [Ingress](docs/ingress.md) - контроллер входящего трафика

Пройдите по ссылкам документации, чтобы узнать, как мы выполнялась установка, и пройти весь процесс по шагам.

### Развертывание Seldon Core

К счастью, это не требует отдельной документации и выполняется в кластере буквально парой команд:

```
kubectl create namespace seldon-system

helm install seldon-core seldon-core-operator \
    --repo https://storage.googleapis.com/seldon-charts \
    --set usageMetrics.enabled=true \
    --namespace seldon-system
```

Проверьте, что Seldon успешно развернулся:

`kubectl get deploy --namespace seldon-system`
