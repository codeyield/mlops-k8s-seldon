## Настройка входящего трафика Kubernetes с Ingress-контроллером

Ingress — это объект API Kubernetes, определяющий правила DNS-маршрутизации для внешнего трафика, поступающего в кластер Kubernetes. Этот объект представляет собой единую точку входа для всего внешнего трафика, поступающего в кластер, и является централизованной точкой конфигурации.

При активации Ingress в MicroK8s, платформа создает и запускает **Nginx Ingress controller pod**. Контроллер Ingress реализует правила маршрутизации Ingress и обеспечивает их согласованное использование.

**Необходимые условия:**

-   MicroK8s установлен
-   Nginx установлен и настроен

## Включите входящий трафик в MicroK8s

`microk8s enable ingress`

Вы должны увидеть сообщение о создании нескольких объектов Kubernetes, необходимых для работы Ingress, в самом конце которого будет строка:

```
...
Ingress is enabled
```

Проверьте, что контроллер запущен:

`microk8s kubectl get pods -A | grep ingress`

Вы должны увидеть приблизительно такую строку:

```
ingress    nginx-ingress-microk8s-controller-*****   1/1   Running 0     5m
```

Запишите имя модуля `nginx-ingress-microk8s-controller-*****`, оно понадобится в дальнейшем.

## Проверьте соединение с Nginx

`curl -v 127.0.0.1`

Вы увидите html-код короткой веб-страницы, на которой, вероятнее всего, будет:

```
404 Not found
```

Тем не менее, этот ответ означает, что Nginx работает правильно (т.к. это не код ошибки сервера 503 и т.д.), просто страница не настроена.

Если это не так, тогда перейдите к [настройке Nginx](nginx.md) и правил брандмауэра, если потребуется.

## Отредактируйте ConfigMap

Когда MicroK8s включает входящий трафик, он создает три ConfigMap в пространстве имен входящего трафика. Чтобы включить работу служб по HTTP, необходимо отредактировать **load balancer ConfigMap**.

Проверьте доступные ConfigMaps командой:

`microk8s kubectl -n ingress get configmap`

Вы должны увидеть примерно такой вывод:

```
NAME                                DATA   AGE
kube-root-ca.crt                    1      12h
nginx-ingress-tcp-microk8s-conf     0      12h
nginx-ingress-udp-microk8s-conf     0      12h
nginx-load-balancer-microk8s-conf   1      12h
```

Эти ConfigMap's были созданы MicroK8s при включении **Ingress**. ConfigMap с именем **nginx-load-balancer-microk8s-conf** необходимо отредактировать.

Следующая команда откроет ConfigMap в редакторе **vim**:

`microk8s kubectl -n ingress edit configmaps nginx-load-balancer-microk8s-conf`

Добавьте следующий фрагмент кода после строки `kind: ConfigMap`:

```
data:
  use-forwarded-headers: "true"
```

У вас должно получиться:

![ConfigMap file after editing.](media/configmap-editing.png)

Теперь сохраните и закройте файл. Здесь несколько команд **vim**, которые могут быстро пригодиться:

  - `i` — перейти в режим редактирования файла (для вставки)
  - `<esc>` — выйти из режима редактирования в командный режим
  - `:wq` — записать файл и выйти из редактора
  - `:q!` — выйти без сохранения

## Проверьте настройку входящего трафика

`microk8s kubectl -n ingress logs <POD_NAME> | grep reload`

Вместо `<POD_NAME>` вставьте имя модуля, которое вы записали ранее. Вы получите примерно такой вывод:

![Configuration changes have been reloaded successfully.](media/nginx-reloaded.png)

Если вы не видите строк об успешной перезагрузке, проверьте изменения в файле ConfigMap, которые вы вносиле выше, и повторите шаги.

## Проверка работы Ingress-контроллера

### Разверните тестовый веб-сервер

Откройте редактор **nano**:

`nano test-server.yaml`

Вставьте код манифеста ниже, сохраните файл и выйдите из редактора:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-server
spec:
  replicas: 1
  selector:
    matchLabels:
      app: test-server
  template:
    metadata:
      labels:
        app: test-server
    spec:
      containers:
      - name: test-server
        image: nginx
---
apiVersion: v1
kind: Service
metadata:
  name: test-server
spec:
  selector:
    app: test-server
  ports:
  - port: 80
```

[Манифест test-server.yaml](../tests/test-server.yaml)

Примените манифест, чтобы развернуть простой веб-сервер Nginx в режиме по умолчанию.

`kubectl apply -f test-server.yaml`

### Создайте ресурс Ingress

`nano test-ingress.yaml`

Вставьте код манифеста ниже, сохраните файл и выйдите из редактора:

```
apiVersion: networking.k8s.io/v1
kind: Ingress  
metadata:
  name: test-ingress
spec:
  rules:
  - http:
      paths:      
      - path: /
        pathType: Prefix
        backend:
          service:
            name: test-server
            port: 
              number: 80
```

[Манифест test-ingress.yaml](../tests/test-ingress.yaml)

Примените манифест, чтобы развернуть ресурс Ingress:

`kubectl apply -f test-ingress.yaml`

### Проверьте работоспособность Ingress

`http://<YOUR_SERVER_IP_ADDRESS>`

Вы должны увидеть веб-страницу с приветствием "`Welcome to nginx!`". Это означает, что Ingress-контроллер работает корректно и проксирует трафик на тестовый веб-сервер.

Если вместо стартовой страницы Nginx вы видите ошибку 404 или 503, то, вероятно, Ingress-контроллер не был правильно установлен или настроен.

### Удалите тестовый деплоймент

Если вы собираетесь разворачивать в кластере реальное приложение, то тестовый деплоймент следует удалить:

`kubectl delete -f test-server.yaml -f test-ingress.yaml`

На этом установка Ingress-контроллера завершена.
