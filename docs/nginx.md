# Установка Nginx на Ubuntu

## Установка

`sudo apt-get update`

`sudo apt-get install nginx`

## Проверка статуса установки

`nginx -v`

Вы увидите сообщение с версией Nginx.

## Проверка работы службы Nginx

`sudo systemctl status nginx`

Если вы не видите статуса **active (running)**, запустите службу Nginx:

`sudo systemctl start nginx`

Чтобы настроить загрузку Nginx при старте системы, введите команду:

`sudo systemctl enable nginx`

Используйте эти команды, если служба не запущена:

`sudo systemctl reload nginx`

Или принудительный рестарт:

`sudo systemctl restart nginx`

## Проверка настройки брандмауэра

Nginx необходим доступ через брандмауэр системы. Для этого Nginx использует профили для стандартного брандмауэра Ubuntu **ufw** (UnComplicated Firewall).

Проверьте доступные профили Nginx:

`sudo ufw app list`

Похожее сообщение означает, что все в порядке и настройка Nginx завершена:

```
Available applications:
  Nginx Full
  Nginx HTTP
  Nginx HTTPS
```

Если вы не видите профили "Nginx HTTP" и "Nginx HTTPS", тогда выполните [настройку правил брандмауэра](docs/ufw.md).

## Финальная проверка Nginx

`curl –i 127.0.0.1`

Вы должны увидеть html-код короткой веб-страницы, содержащей следующий текст:

```
Welcome to nginx!
If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.
```

Также откройте ссылку в браузере:

`http://<YOUR_SERVER_IP_ADDRESS>`

Вы должны увидеть веб-страницу с таким сообщением:

![Welcome to nginx](media/welcome-to-nginx.png)

Это означает, что все в порядке и вы можете перейти к установке [Ingress контроллера](ingress.md).
