# Установка microk8s на Ubuntu

## Установка

`sudo apt-get update`

`sudo snap install microk8s --classic --channel=1.29`

## Создание и присоединение группы

```
sudo usermod -a -G microk8s $USER
sudo mkdir -p ~/.kube
sudo chown -f -R $USER ~/.kube
su - $USER
```

## Проверка статуса установки

`microk8s status --wait-ready`

`microk8s kubectl get nodes`

## Добавление псевдонимов для команд

Делайте это, только если у вас ранее НЕ БЫЛ установлен **kubectl** в системе! Проверьте наличие предыдущей установки командой `kubectl version` перед созданием псевдонима.

`alias kubectl='microk8s kubectl'`

**Helm** ad-on по умолчанию активен после установки microk8s. Проверьте это командой `microk8s status`. Если вы видите **Helm** в разделе "enabled", то вы можете создать псевдоним для него.

`alias helm='microk8s helm'`

При проблемах с созданием псевдонимов просто используйте команды:

`microk8s kubectl ...`

`microk8s helm ...`

## Активация рекомендуемых add-ons

```
microk8s enable dns
microk8s enable hostpath-storage
```

## Установка Ingress-контроллера

*Перед установкой Ingress убедитесь, что на вашей машине [установлен и корректно настроен Nginx сервер](nginx.md).*

Если с Nginx все в порядке, тогда переходите к [настройке Ingress-контроллера](ingress.md).
